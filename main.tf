
module "vpc1" {
    source = "./module/iaac"
    providers = {
      aws = aws.as1
    }
    identifier = "vpc1"
    vpc_cidr = "10.0.0.0/16"
    vpc_public_subnet_cidr = "10.0.0.0/20"
    vpc_private_subnet_cidr = "10.0.16.0/20"
  
}

module "vpc2" {
    source = "./module/iaac"
    providers = {
      aws = aws.as1
    }
    identifier = "vpc2"
    vpc_cidr = "172.31.0.0/16"
    vpc_public_subnet_cidr = "172.31.0.0/20"
    vpc_private_subnet_cidr = "172.31.16.0/20"
  
}