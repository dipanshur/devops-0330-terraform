terraform {
  backend "s3" {
    bucket = "devops-0330-backend"
    key = "tfstate/terraform.tfstate"
    region = "ap-south-1"
    
  }
}