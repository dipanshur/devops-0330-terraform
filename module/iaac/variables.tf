variable "identifier" {
    description = "Used to identify the specific resources"

  
}

variable "vpc_cidr" {
  description = "This is a variable that stores CIDR block for VPC"
}

variable "vpc_public_subnet_cidr" {
    description = "value of public subnet cidr in which we want to create Public Subnets."
  
}

variable "vpc_private_subnet_cidr" {
    description = "value of private subnet cidr in which we want to create Public Subnets."
  
}